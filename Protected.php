<?php
session_start ();

?>

<!DOCTYPE html>
<html>
<head>
<title>Page protégée</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Espace administrateur top secret</h1>
    <h3>Mon login : <?php echo $_SESSION['name']?></h3>
    <form action="logoff.php" method="post">
        <button type="submit">Deconnection</button></form>
    

<div class = "flex_horizontal">        
<div class="table">
<h4>Current admins list</h4>

<?php
    $servername = 'localhost';
    $username = 'Bob';
    $password = 'toto123';
    $dbname = 'login_database';

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT * FROM login_table";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {

  while($row = mysqli_fetch_assoc($result)) {
    echo "id: " . $row["id"]. " - login: " . $row["login"]. " - password: " . $row["password"]. "<br>";
  }
} else {
  echo "0 results";
}

mysqli_close($conn);
?> 

<h4>Changer votre mot de passe</h4>
<form action="change_password.php" method="post">
        <div>
            <label for="password">Mot de passe :</label>
            <input type="text" id="new_password" name="new_password"required="true" placeholder="Nouveau mot de passe">
        </div>

        <div id="bouton submit" class="button">
            <button type="submit">Changer votre mot de passe</button>
        </div>
    </form>




</div>


    <div class="add_admin">
    <h4>Créer un compte administrateur</h4>
    <form action="add_admin.php" method="post">
        <div>
            <label for="name">Nom d'utilisateur :</label>
            <input type="text" id="name" name="user_name" value="" required="true" placeholder="Login">

        </div>
        <div>
            <label for="password">Mot de passe :</label>
            <input type="password" id="password_input_box" name="password"required="true" placeholder="Password">
        </div>

        <div id="bouton submit" class="button">
            <button type="submit">Cliquez pour ajouter un compte administrateur</button>
        </div>
    </form>

    <h4>Supprimer un compte</h4>

<form action="remove_admin.php" method="post">
        <div>
            <label for="name">via son id :</label>
            <input type="text" id="id" name="id" value="0" required="false" placeholder="id du login à supprimer">

        </div>
        <div id="bouton submit" class="button">
            <button type="submit">Supprimer le compte</button>
        </div>
    </form>
    </div>

</div>




</body>
</html> 
